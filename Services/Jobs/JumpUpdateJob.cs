using System;
using System.Threading.Tasks;
using EDCS.Model;
using Quartz;
using Serilog;

namespace EDCS.Services.Jobs
{
    public class JumpUpdateJob : IJob
    {
        private JumpService _jumpService;

        public JumpUpdateJob(JumpService jumpService)
        {
            _jumpService = jumpService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() => {
                var jumps = _jumpService.GetActiveJumps();
                Log.Debug("Active Jumps to check: {0}", jumps.Count);

                foreach (var jump in jumps)
                {
                    Log.Debug($"{jump.Id}: {jump.CarrierId} | {jump.Destination} | {jump.Status} | {jump.JumpTime}");
                    if (jump.Status is JumpStatus.Plotted && jump.IsPastLockdownTime() && !jump.IsPastCompleteTime())
                    {
                        // Update jump to be in lockdown
                        Log.Debug("Updating jump {0} to Lockdown", jump.Id);
                        jump.Status = JumpStatus.Lockdown;
                        _jumpService.UpdateJump(jump);
                        _jumpService.UpdateJumpAnnouncement(jump);
                    }
                    else if (jump.Status is JumpStatus.Plotted or JumpStatus.Lockdown && jump.IsPastCompleteTime())
                    {
                        // Update the jump to be completed
                        Log.Debug("Updating jump {0} to Complete", jump.Id);
                        jump.Status = JumpStatus.Complete;
                        _jumpService.UpdateJump(jump);
                        _jumpService.UpdateJumpAnnouncement(jump);
                    }
                    else
                    {
                        // do nothing
                    }
                }
            });
        }
    }
}