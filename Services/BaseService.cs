using DSharpPlus;
using MongoDB.Driver;

namespace EDCS.Services
{
    public abstract class BaseService
    {
        private protected MongoClient _mongoClient {get; private set;}
        private protected DiscordClient _discordClient {get; private set;}

        public BaseService(MongoClient dbClient, DiscordClient discordClient) {
            _mongoClient = dbClient;
            _discordClient = discordClient;
        }
    }
}