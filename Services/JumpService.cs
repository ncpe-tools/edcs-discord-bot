using System;
using System.Collections.Generic;
using System.Linq;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;
using EDCS.Model;
using MongoDB.Driver;
using Serilog;

namespace EDCS.Services
{
    /// <summary>
    /// Provides methods for creating and interacting with carrier jumps.
    /// </summary>
    public class JumpService : BaseService
    {

        private IMongoCollection<Jump> _jumpCollection;
        private CarrierService _carrierService;
        private EmojiService _emojiService;

        /// <summary>
        /// Initializes a new instance of <see cref="JumpService"/>.
        /// </summary>
        /// <param name="dbClient">The <see cref="MongoClient"/> to use for database interactions.</param>
        /// <param name="discordClient">The <see cref="DiscordClient"/> for interacting with Discord.</param>
        /// <param name="carrierService">The <see cref="CarrierService"/> for retrieveing carrier information.</param>
        /// <param name="emojiService">The <see cref="EmojiService"/> for retrieveing custom Discord emojis.</param>
        public JumpService(MongoClient dbClient, DiscordClient discordClient, CarrierService carrierService, EmojiService emojiService) : base(dbClient, discordClient)
        {
            _carrierService = carrierService;
            _emojiService = emojiService;
            _jumpCollection = dbClient.GetCollection<Jump>();
        }

        /// <summary>
        /// Gets a list of jumps for a carrier.
        /// </summary>
        /// <param name="carrierId">The carrier id.</param>
        /// <returns>The list of <see cref="Jump"/>s.</returns>
        public List<Jump> GetJumpsForCarrier(string carrierId)
        {
            var carrier = _carrierService.GetCarrier(carrierId);
            if (carrier is not null)
            {
                return _jumpCollection.Find(x => x.CarrierId == carrierId).ToList();
            }

            return new List<Jump>();
        }

        /// <summary>
        /// Gets the most recent jump for a carrier.
        /// </summary>
        /// <param name="carrierId">The carrier id.</param>
        /// <returns>The most recent jump; null if one does not exist.</returns>
        public Jump GetLatestJumpForCarrier(string carrierId)
        {
            var carrier = _carrierService.GetCarrier(carrierId);
            if (carrier is not null)
            {
                return _jumpCollection.Find(x => x.CarrierId == carrierId).SortByDescending(x => x.Id).FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// Gets a jump from the database.
        /// </summary>
        /// <param name="jumpId">The database id of the jump.</param>
        /// <returns>The requested jump or null if it does not exist.</returns>
        public Jump GetJump(string jumpId)
        {
            return _jumpCollection.Find(x => x.Id.ToString() == jumpId).SingleOrDefault();
        }

        /// <summary>
        /// Gets a list of all jumps with a <see cref="JumpStatus"/> of "Plotted" or "Lockdown".
        /// </summary>
        /// <returns>The list of jumps.</returns>
        public List<Jump> GetActiveJumps()
        {
            return _jumpCollection.Find(x => x.Status == JumpStatus.Plotted || x.Status == JumpStatus.Lockdown).ToList();
        }

        /// <summary>
        /// Adds a new jump to the database.
        /// </summary>
        /// <param name="jump">The jump to add.</param>
        public void AddJump(Jump jump)
        {
            _jumpCollection.InsertOne(jump);
        }

        /// <summary>
        /// Update a jump in the database.
        /// </summary>
        /// <param name="jump">The jump to update.</param>
        /// <returns>The updated jump returned by the database.</returns>
        public Jump UpdateJump(Jump jump)
        {
            if (jump.IsPastLockdownTime() && jump.Status is JumpStatus.Plotted)
            {
                jump.Status = JumpStatus.Lockdown;
            }
            else if (!jump.IsPastLockdownTime() && jump.Status is JumpStatus.Lockdown)
            {
                jump.Status = JumpStatus.Plotted;
            }

            if (jump.Status is JumpStatus.Complete)
            {
                var carrier = _carrierService.GetCarrier(jump.CarrierId);
                carrier.Location = new Location
                {
                    StarSystem = jump.Destination,
                    Source = "Bot Jump"
                };
                _carrierService.UpdateCarrier(carrier);
            }

            return _jumpCollection.FindOneAndReplace(j => j.Id == jump.Id, jump);
        }

        /// <summary>
        /// Posts a new jump announcement message to the carrier's configured Discord channel.
        /// </summary>
        /// <param name="jump">The jump to announce.</param>
        /// <remarks>If the carrier does not have a channel or has notification muted, this method will do nothing.</remarks>
        public async void SendJumpAnnouncement(Jump jump)
        {
            var carrier = _carrierService.GetCarrier(jump.CarrierId);
            var channel = carrier.Channel;
            if (carrier is null || channel is null || carrier.Channel.SilentRunning)
            {
                return;
            }
            else
            {
                var dc = await _discordClient.GetChannelAsync(channel.ChannelId);
                var msgBuilder = new DiscordMessageBuilder();
                msgBuilder.WithContent(BuildJumpMessage(jump, dc));

                var msg = await _discordClient.SendMessageAsync(dc, msgBuilder);
                jump.MessageId = msg.Id;
                UpdateJump(jump);
            }
        }

        /// <summary>
        /// Updates the message for a jump if one exists.
        /// </summary>
        /// <param name="jump">The jump to update the message of.</param>
        public async void UpdateJumpAnnouncement(Jump jump)
        {
            var carrier = _carrierService.GetCarrier(jump.CarrierId);
            if (jump.MessageId != default)
            {
                var ch = await _discordClient.GetChannelAsync(carrier.Channel.ChannelId);
                try
                {
                    var msg = await ch.GetMessageAsync(jump.MessageId);
                    var newContent = BuildJumpMessage(jump, ch);
                    await msg.ModifyAsync(newContent);
                }
                catch (Exception ex) when (ex is NotFoundException || ex is BadRequestException)
                {
                    Log.Warning("Message {0} not found. Removing from database.", jump.MessageId);
                    jump.MessageId = default;
                    UpdateJump(jump);
                }
            }
        }

        /// <summary>
        /// Builds a jump info message
        /// </summary>
        /// <param name="jump">The jump to build the message from.</param>
        /// <param name="channel">The channel the message will be sent to. Used for external emoji permission check.</param>
        /// <returns>A formatted jump message</returns>
        private string BuildJumpMessage(Jump jump, DiscordChannel channel)
        {
            var carrier = _carrierService.GetCarrier(jump.CarrierId);
            var externalEmoji = channel.PermissionsFor(channel.Guild.CurrentMember).HasPermission(Permissions.UseExternalEmojis);

            DiscordEmoji jumpEmoji;
            DiscordEmoji lockdownEmoji;
            DiscordEmoji doneEmoji;
            DiscordEmoji cancelEmoji;

            if (externalEmoji) {
                DiscordEmoji.TryFromName(_discordClient, Constants.Emoji.FCJump, out jumpEmoji);
                DiscordEmoji.TryFromName(_discordClient, Constants.Emoji.FCDone, out doneEmoji);
                DiscordEmoji.TryFromName(_discordClient, Constants.Emoji.FCLockdown, out lockdownEmoji);
                DiscordEmoji.TryFromName(_discordClient, Constants.Emoji.FCCancel, out cancelEmoji);
            } else {
                var emojis = channel.Guild.GetEmojisAsync().Result;
                jumpEmoji = emojis.SingleOrDefault(x => x.Name == Constants.Emoji.FCJump);
                lockdownEmoji = emojis.SingleOrDefault(x => x.Name == Constants.Emoji.FCLockdown);
                doneEmoji = emojis.SingleOrDefault(x => x.Name == Constants.Emoji.FCDone);
                cancelEmoji = emojis.SingleOrDefault(x => x.Name == Constants.Emoji.FCCancel);
            }

            var destination = Formatter.Bold(jump.Destination);
            destination = jump.Distance.HasValue ? $"{destination} ({jump.Distance} ly)" : destination;
            switch (jump.Status)
            {
                case JumpStatus.Cancelled:
                    return $"{cancelEmoji} {Formatter.Bold(carrier.Name)} has cancelled a jump to {destination}";
                case JumpStatus.Complete:
                    return $"{doneEmoji} {Formatter.Bold(carrier.Name)} has completed a jump to {destination} {Formatter.Timestamp(jump.JumpTime)}";
                case JumpStatus.Lockdown:
                    return $"{lockdownEmoji} {Formatter.Bold(carrier.Name)} is jumping to {destination} {Formatter.Timestamp(jump.JumpTime)} ({Formatter.Bold("In Lockdown")})";
                case JumpStatus.Plotted:
                default:
                    var lockdown = Formatter.Italic($"(Lockdown {Formatter.Timestamp(jump.JumpTime - jump.LockdownAt)})");
                    return $"{jumpEmoji} {Formatter.Bold(carrier.Name)} is jumping to {destination} {Formatter.Timestamp(jump.JumpTime)} {lockdown}";
            }
        }
    }
}