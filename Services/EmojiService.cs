using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;

namespace EDCS.Services
{
    public class EmojiService : BaseService
    {
        private DiscordClient _client;

        public EmojiService(MongoClient dbClient, DiscordClient discordClient) : base(dbClient, discordClient)
        {
            _client = discordClient;
        }

        public async Task<bool> IsEmojiAvailable(DiscordGuild guild, string emoji) {
            var emojis = await guild.GetEmojisAsync();
            return emojis.Select(x => x.Name == emoji).Any();
        }

        public async Task<bool> IsEmojiAvailable(ulong guildId, string emoji) {
            var guild = await _client.GetGuildAsync(guildId);
            var emojis = await guild.GetEmojisAsync();
            return emojis.Select(x => x.Name == emoji).Any();
        }

        public async Task<DiscordEmoji> GetEmojiAsync(DiscordGuild guild, string emoji) {
            var emojis = await guild.GetEmojisAsync();
            return emojis.Where(x => x.Name == emoji).FirstOrDefault();            
        }

        public async Task<DiscordEmoji> GetEmojiAsync(ulong guildId, string emoji) {
            var guild = await _client.GetGuildAsync(guildId);
            return await GetEmojiAsync(guild, emoji);
        }
    }
}