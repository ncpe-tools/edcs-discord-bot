using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DSharpPlus;
using DSharpPlus.Entities;
using EDCS.Model;
using MongoDB.Driver;
using Serilog;

namespace EDCS.Services
{
    /// <summary>
    /// Provides methods for creating and interacting with fleet carriers.
    /// </summary>
    public class CarrierService : BaseService
    {
        private IMongoCollection<Carrier> _carrierCollection;

        public CarrierService(MongoClient dbClient, DiscordClient discordClient) : base(dbClient, discordClient) {
            _carrierCollection = dbClient.GetCollection<Carrier>();
        }

        /// <summary>
        /// Get a carrier from the database
        /// </summary>
        /// <param name="carrierId">The ingame ID of the carrier</param>
        /// <returns>The populated Carrier object; null if it does not exist in the database</returns>
        public Carrier GetCarrier(string carrierId) {
            var carrier = _carrierCollection.Find(x => x.Id == carrierId).SingleOrDefault();
            return carrier;
        }

        /// <summary>
        /// Get a carrier from the database
        /// </summary>
        /// <param name="marketId">The hidden ID of the carrier</param>
        /// <returns>The populated Carrier object; null if it does not exist in the database</returns>
        public Carrier GetCarrier(ulong marketId) {
            var carrier = _carrierCollection.Find(x => x.MarketId == marketId).SingleOrDefault();
            return carrier;
        }
        
        /// <summary>
        /// Gets a single carrier based on a filter
        /// </summary>
        /// <param name="filter">The filter to apply.</param>
        /// <returns>The single carrier matching the filter, otherwise null</returns>
        public Carrier GetCarrierByFilter(Expression<Func<Carrier,bool>> filter) {
            return _carrierCollection.Find(filter).SingleOrDefault();
        }

        /// <summary>
        /// Gets the carrier associated with a Discord channel
        /// </summary>
        /// <param name="channel">The Discord channel</param>
        /// <returns>The associated carrier or null if one does not exist</returns>
        public Carrier GetCarrierByChannel(DiscordChannel channel) {
            return _carrierCollection.Find(x => x.Channel.GuildId == channel.GuildId && x.Channel.ChannelId == channel.Id).SingleOrDefault();
        }

        /// <summary>
        /// Gets all carriers associated with a given Discord guild
        /// </summary>
        /// <param name="guildId">The id of the guild</param>
        /// <returns>A list of carriers in the guild</returns>
        public List<Carrier> GetAllCarriersByGuild(ulong guildId) {
            return _carrierCollection.Find(x => x.GuildId == guildId).ToList();
        }

        /// <summary>
        /// Gets all carriers registered in the bot
        /// </summary>
        public List<Carrier> GetAllCarriers() {
            return _carrierCollection.Find(x => true).ToList();
        }

        /// <summary>
        /// Checks if a given carrier id is registered in the database.
        /// </summary>
        /// <param name="carrierId">The carrier id to check</param>
        /// <returns>True if the carrier is in the database. False otherwise.</returns>
        public bool CarrierExists(string carrierId) {
            return _carrierCollection.Find(x => x.Id == carrierId).Any();
        }

        /// <summary>
        /// Checks if a given discord channel is mapped to a carrier. Optionally checks if the channel is mapped to a given carrier
        /// </summary>
        /// <param name="channel">The Discord channel to check</param>
        /// <param name="carrierId">A specific carrier to check for. If null, will check across all carriers.</param>
        /// <returns></returns>
        public bool IsCarrierChannel(DiscordChannel channel, string carrierId = null){
            if (carrierId == null) {
                var mappedCarrier = _carrierCollection.Find(x => x.Channel.GuildId == channel.GuildId && x.Channel.ChannelId == channel.Id).SingleOrDefault();
                return mappedCarrier != null;
            } else {
                var carrier = GetCarrier(carrierId);
                if (carrier == null || carrier.Channel == null)
                    return false;
                return carrier.Channel.GuildId == channel.GuildId && carrier.Channel.ChannelId == channel.Id;                            
            }
        }

        /// <summary>
        /// Update a carrier in the database
        /// </summary>
        /// <param name="carrier">The updated carrier object with a populated MongoId field.</param>
        /// <returns>The updated carrier object</returns>
        public Carrier UpdateCarrier(Carrier carrier) {
            if (carrier.Channel is not null && carrier.Location is not null) {
                // Update the channel topic to include the location
                var text = $"Current Location: **{carrier.Location.StarSystem}**";
                UpdateCarrierChannelTopic(carrier.Channel, text);
            }
            return _carrierCollection.FindOneAndReplace(c => c.MongoId == carrier.MongoId, carrier);
        }   

        /// <summary>
        /// Registers a carrier in the database
        /// </summary>
        /// <param name="carrier">The carrier object to register</param>
        /// <returns>True if the carrier was registered. False if a carrier with the same ID already exists.</returns>
        public bool RegsiterCarrier(Carrier carrier) {
            if (CarrierExists(carrier.Id)) return false;
            
            _carrierCollection.InsertOne(carrier);
            return true;
        }

        /// <summary>
        /// Unregsiter a carrier, removing it from the database. Will also remove any bot created channels for the carrier.
        /// </summary>
        /// <param name="carrierId">The ID of the carrier to remove</param>
        /// <returns>True if the carrier was unregistered successfully. False otherwise.</returns>
        public bool UnregisterCarrier(string carrierId) {
            if (!CarrierExists(carrierId)) return false;

            _carrierCollection.DeleteOne(x => x.Id == carrierId);
            return true;
        }

        /// <summary>
        /// Updates the channel topic of a <see cref="CarrierChannel"/>.
        /// </summary>
        /// <param name="channel">The channel to update.</param>
        /// <param name="topic">The new channel topic.</param>
        /// <remarks>Note that due to Discord rate limits, there may be a delay of up to two minutes before the change is reflected in Discord.</remarks>
        private async void UpdateCarrierChannelTopic(CarrierChannel channel, string topic) {
            var dc = await _discordClient.GetChannelAsync(channel.ChannelId);
            
            if (dc.PermissionsFor(dc.Guild.CurrentMember).HasPermission(Permissions.ManageChannels)) {
                await dc.ModifyAsync(x => x.Topic = topic);
            } else {
                Log.Error($"Unable to set channel topic. Missing ManageChannels permission for {dc.Guild.Name} #{dc.Name}");
            }

        }

        public CarrierAccess ConvertDockingAccessEnum(string DockingAccess) {
            switch (DockingAccess)
            {
                case "all": return CarrierAccess.All;
                case "none": return CarrierAccess.None;
                case "friends": return CarrierAccess.Friends;
                case "squadron": return CarrierAccess.Squadron;
                case "squadronfriends": return CarrierAccess.SquadronFriends;
                default: return CarrierAccess.Unknown;
            }
        }
    }
}