using System.Numerics;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace EDCS
{
    public class BsonVectorSerializer : StructSerializerBase<Vector3>
    {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Vector3 value)
        {
            var bsonWriter = context.Writer;
            if (value == null)
            {
                bsonWriter.WriteNull();
            }
            else
            {
                bsonWriter.WriteStartDocument();
                bsonWriter.WriteDouble("X", value.X);
                bsonWriter.WriteDouble("Y", value.Y);
                bsonWriter.WriteDouble("Z", value.Z);
                bsonWriter.WriteEndDocument();
            }
        }

        public override Vector3 Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var obj = (object)(new Vector3());
            var actualType = args.NominalType;
            var bsonReader = context.Reader;

            bsonReader.ReadStartDocument();

            while (bsonReader.ReadBsonType() != BsonType.EndOfDocument)
            {
                var name = bsonReader.ReadName(Utf8NameDecoder.Instance);

                var field = actualType.GetField(name);
                if (field != null)
                {
                    var value = BsonSerializer.Deserialize(bsonReader, field.FieldType);
                    field.SetValue(obj, value);
                }

                var prop = actualType.GetProperty(name);
                if (prop != null)
                {
                    var value = BsonSerializer.Deserialize(bsonReader, prop.PropertyType);
                    prop.SetValue(obj, value, null);
                }
            }

            bsonReader.ReadEndDocument();

            return (Vector3)obj;
        }
    }

    public class BsonNullableVectorSerializer : NullableSerializer<Vector3>
    {
        public BsonNullableVectorSerializer() : base(new BsonVectorSerializer()) {}
    }

}