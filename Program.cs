﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using EDCS.Commands;
using EDCS.Commands.Attributes;
using EDCS.Services;
using EDCS.Services.Jobs;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Impl;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace EDCS
{
    class Program
    {

        public readonly EventId BotEventId = new EventId(42, "EDCS-Dev");

        public DiscordClient Client { get; set; }
        public SlashCommandsExtension Commands { get; set; }

        static void Main(string[] args)
        {
            var prog = new Program();
            prog.RunBotAsync().GetAwaiter().GetResult();
        }

        public async Task RunBotAsync()
        {
            var token = Environment.GetEnvironmentVariable("BOT_TOKEN");

            var guildIdEnv = Environment.GetEnvironmentVariable("GUILD_ID");

            var logLevelEnv =  Environment.GetEnvironmentVariable("LOG_LEVEL");
            
            var commandGuildIds = new List<ulong>();

            foreach (var id in guildIdEnv.Split(','))
            {
                ulong? parsedId = ulong.TryParse(id, out var i) ? i : null;
                if (parsedId.HasValue) {
                    commandGuildIds.Add(parsedId.Value);
                }
            }

            if (token is null)
            {
                throw new Exception("Bot token missing. Please populate the BOT_TOKEN environment variable with a valid bot token.");
            }

            var cfg = new DiscordConfiguration
            {
                Token = token,
                TokenType = TokenType.Bot,

                AutoReconnect = true,
                MinimumLogLevel = Enum.TryParse<LogLevel>(logLevelEnv, true, out var j) ? j : LogLevel.Information,
            };

            this.Client = new DiscordClient(cfg);

            var mongo = new MongoClient(Environment.GetEnvironmentVariable("MONGO_URI"), Environment.GetEnvironmentVariable("MONGO_DB"));
            var properties = new NameValueCollection { { $"{StdSchedulerFactory.PropertyJobStorePrefix}.{StdSchedulerFactory.PropertyDataSourceConnectionString}", Environment.GetEnvironmentVariable("MONGO_URI") } };


            var loggingLevel = new LoggingLevelSwitch();
            var parsedLevel = Enum.TryParse<LogEventLevel>(logLevelEnv, true, out var k) ? k : LogEventLevel.Information;
            loggingLevel.MinimumLevel = parsedLevel;

            Log.Logger = new LoggerConfiguration().MinimumLevel.ControlledBy(loggingLevel).WriteTo.Console().CreateLogger();

            var services = new ServiceCollection();
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.AddSerilog(dispose: true);
            });
            services.AddSingleton<MongoClient>(mongo);
            services.AddSingleton<CarrierService>();
            services.AddSingleton<JumpService>();
            services.AddSingleton<EmojiService>();
            services.AddSingleton<DiscordClient>(Client);

            services.Configure<QuartzOptions>(options =>
            {
                options.Scheduling.IgnoreDuplicates = true; // default: false
                options.Scheduling.OverWriteExistingData = true; // default: true
            });

            services.AddQuartz(q =>
            {
                q.SchedulerName = "EDCS Bot Scheduler";
                q.SchedulerId = "EDCS-Sccheduler";
                q.UseMicrosoftDependencyInjectionJobFactory();
                q.UseSimpleTypeLoader();
                q.UseInMemoryStore();
                q.UseDefaultThreadPool(maxConcurrency: 10);

                q.ScheduleJob<JumpUpdateJob>(trigger => trigger
                    .WithIdentity("JumpUpdater")
                    .WithSimpleSchedule(x => x.WithIntervalInSeconds(15).RepeatForever())
                    .StartNow()
                );
            });

            services.AddTransient<JumpUpdateJob>();

            var provider = services.BuildServiceProvider();
            var slash = this.Client.UseSlashCommands(new SlashCommandsConfiguration()
            {
                Services = provider
            });

            this.Client.UseInteractivity(new DSharpPlus.Interactivity.InteractivityConfiguration()
            {
                ResponseBehavior = DSharpPlus.Interactivity.Enums.InteractionResponseBehavior.Ack
            });

            slash.SlashCommandErrored += async (s, e) =>
            {
                if (e.Exception is SlashExecutionChecksFailedException slex)
                {
                    foreach (var check in slex.FailedChecks)
                        if (check is RequireCarrierOwnerAttribute att)
                            await e.Context.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent($"Only the carrier owner or an admin can run this command!").AsEphemeral());
                }
            };

            if (commandGuildIds.Count > 0) {
                Log.Information($"Registering commands under guilds: {string.Join(',', commandGuildIds)}");
                foreach (var guildId in commandGuildIds)
                {
                    slash.RegisterCommands<RegistrationCommands>(guildId);
                    slash.RegisterCommands<CarrierCommands>(guildId);
                }
            } else {
                Log.Information("Registering commands globally.");
                slash.RegisterCommands<RegistrationCommands>();
                slash.RegisterCommands<CarrierCommands>();
            }

            await this.Client.ConnectAsync();

            // Start the scheduler after the bot connects
            var sch = await provider.GetService<ISchedulerFactory>().GetScheduler();
            await sch.Start();

            InitializeApi(services);
            await Task.Delay(-1);
        }

        private void InitializeApi(ServiceCollection services)
        {
            var builder = WebApplication.CreateBuilder();
            builder.Services.AddControllers();
            builder.Services.Add(services);
            
            var app = builder.Build();

            if (builder.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.MapControllers();

            app.RunAsync();
        }

        private Task Client_Ready(DiscordClient sender, ReadyEventArgs e)
        {
            // let's log the fact that this event occured
            sender.Logger.LogInformation(BotEventId, "Client is ready to process events.");

            // since this method is not async, let's return
            // a completed task, so that no additional work
            // is done
            return Task.CompletedTask;
        }
    }
}
