using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using EDCS.Model;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace EDCS.Commands.Autocomplete
{
    /// <summary>
    /// Provides Discord autocompletion for <see cref="Carrier"/> IDs.
    /// </summary>
    class CarrierAutocompleteProvider : IAutocompleteProvider
    {
        public virtual async Task<IEnumerable<DiscordAutoCompleteChoice>> Provider(AutocompleteContext ctx)
        { 
            return await Task.Run(() => {
                return Operation(x => x.Id.StartsWith(ctx.OptionValue.ToString()), ctx);
            });
        }

        protected IEnumerable<DiscordAutoCompleteChoice> Operation(System.Linq.Expressions.Expression<System.Func<Carrier, bool>> filter, AutocompleteContext ctx)
        {
                
                var mongo =  ctx.Services.GetService<MongoClient>();
                if (mongo == null) 
                {
                    return new List<DiscordAutoCompleteChoice>();
                }

                var collection = mongo.GetCollection<Carrier>();
                var results = collection.Find(filter).ToList();

                return results.Select(x => new DiscordAutoCompleteChoice($"{x.Id} - {x.Name}", x.Id));
        }
    }

    /// <summary>
    /// Provides Discord autocompletion for <see cref="Carrier"/> IDs owned by the user.
    /// </summary>
    class OwnedCarrierAutocompleteProvider : CarrierAutocompleteProvider
    {
        public override async Task<IEnumerable<DiscordAutoCompleteChoice>> Provider(AutocompleteContext ctx)
        { 
            return await Task.Run(() => {
                return Operation(x => x.Id.StartsWith(ctx.OptionValue.ToString()) && x.OwnerId.Equals(ctx.Member.Id), ctx);
            });
        }
    }
}