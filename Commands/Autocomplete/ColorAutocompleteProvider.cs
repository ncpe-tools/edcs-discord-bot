using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;

namespace EDCS.Commands.Autocomplete
{
    public class ColorAutocompleteProvider : IAutocompleteProvider
    {
        public async Task<IEnumerable<DiscordAutoCompleteChoice>> Provider(AutocompleteContext ctx)
        {
            return await Task.Run(() => {
                var input = ctx.OptionValue.ToString().ToLower();
                var discordColor = new DiscordColor();
                var properties = typeof(DiscordColor).GetProperties().Where(x => x.PropertyType == typeof(DiscordColor)).ToDictionary(x => x.Name, x => (DiscordColor) x.GetValue(discordColor));
                properties.Add("Default", new DiscordColor("e3e5e8"));

                return properties.Where(x => x.Key.ToLower().StartsWith(input)).Select(x => new DiscordAutoCompleteChoice(x.Key, x.Value.ToString())).Take(25);
            });
        }
    }
}