using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using EDCS.Commands.Attributes;
using EDCS.Commands.Autocomplete;
using EDCS.Model;
using EDCS.Services;
using MongoDB.Driver;
using Serilog;

namespace EDCS.Commands
{
    /// <summary>
    /// Provides commands related to fleet carriers.
    /// </summary>
    class CarrierCommands : ApplicationCommandModule
    {
        public MongoClient mongoClient { private get; set; }
        public CarrierService carrierService { private get; set; }
        public JumpService jumpService { private get; set; }

        [SlashCommand("undock", "Mark yourself as undocked from a carrier.")]
        public async Task Undock(InteractionContext ctx)
        {
            var carrier = carrierService.GetCarrierByFilter(x => x.GuildId == ctx.Guild.Id && x.Passengers.Contains(ctx.Member.Id));
            if (carrier is null)
            {
                await ctx.CreateResponseAsync("You are not docked at a carrier", ephemeral: true);
                return;
            }

            var result = carrier.RemovePassenger(ctx.Member.Id);

            if (result)
            {
                carrierService.UpdateCarrier(carrier);
                await ctx.CreateResponseAsync($"You have undocked from _{carrier.Name}_", ephemeral: true);
            }
            else
            {
                await ctx.CreateResponseAsync("Unable to remove you from the carrier.", ephemeral: true);
            }
        }

        [SlashCommand("dock", "Mark yourself as docked at a carrier. Use in a carrier channel to dock at that carrier by default.")]
        public async Task Dock(InteractionContext ctx, [Option("carrier", "The id of the carrier to dock at."), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId = null)
        {
            // Get the carrier information
            Carrier carrier;

            var currentChannel = ctx.Channel.Id;
            var carriers = carrierService.GetAllCarriersByGuild(ctx.Guild.Id);

            if (carrierId is null)
            {
                carrier = carrierService.GetCarrierByChannel(ctx.Channel);
            }
            else
            {
                carrier = carrierService.GetCarrier(carrierId);
            }

            //Check if the user is on a different carrier already
            var other = carriers.SingleOrDefault(c => c.HasPassenger(ctx.Member.Id));
            if (other is not null && other.Id != carrier.Id)
            {
                await ctx.CreateResponseAsync($"You are already docked on the carrier _{other.Name}_. Please undock from that carrier first.", ephemeral: true);
                return;
            }

            if (carrier is not null)
            {
                var result = carrier.AddPassenger(ctx.Member.Id);

                if (result)
                {
                    carrierService.UpdateCarrier(carrier);
                    await ctx.CreateResponseAsync($"You are now docked at _{carrier.Name}_", ephemeral: true);
                }
                else
                {
                    await ctx.CreateResponseAsync("You are already on this carrier");
                }
            }
            else
            {
                await ctx.CreateResponseAsync("Carrier not found. Please pass the carrier id or use in a carrier channel.", ephemeral: true);
            }
        }

        [SlashCommand("silent-running", "Toggle notifications for a carrier")]
        [RequireCarrierOwner]
        public async Task SilentRunning(InteractionContext ctx, [Option("carrier", "The id of the carrier."), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId = null)
        {
            var carrier = carrierId is null ? carrierService.GetCarrierByChannel(ctx.Channel) : carrierService.GetCarrier(carrierId);
            if (carrier is not null && carrier.Channel is not null)
            {
                var silent = carrier.Channel.SilentRunning;
                carrier.Channel.SilentRunning = !silent;
                carrierService.UpdateCarrier(carrier);
                await ctx.CreateResponseAsync($"Silent running {(silent ? "disabled" : "enabled")} for **{carrier.Name}**.", ephemeral: true);
            }
            else if (carrier != null && carrier.Channel == null)
            {
                await ctx.CreateResponseAsync("That carrier does not have a channel configured for notifications", ephemeral: true);
            }
            else
            {
                await ctx.CreateResponseAsync("Carrier not found. Please pass the carrier id or use in a carrier channel.", ephemeral: true);
            }
        }

        [SlashCommand("info", "Get information on a carrier")]
        public async Task Info(InteractionContext ctx, [Option("carrier", "The id of the carrier to get information for."), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId = null)
        {
            var carrier = carrierId is null ? carrierService.GetCarrierByChannel(ctx.Channel) : carrierService.GetCarrier(carrierId);
            if (carrier is null)
            {
                await ctx.CreateResponseAsync("Carrier not found. Please pass the carrier id or use in a carrier channel.");
                return;
            }

            var owner = await ctx.Client.GetUserAsync(carrier.OwnerId);
            var jump = jumpService.GetLatestJumpForCarrier(carrier.Id);

            var builder = new DiscordEmbedBuilder();
            builder.WithTitle($"{carrier.Name} - {carrier.Id}");
            builder.WithUrl(carrier.GetInaraUrl());

            var location = "Unknown";
            if (carrier.Location is not null)
            {
                var locUrl = new Uri(Constants.Inara.StarSystemBaseUrl + carrier.Location.StarSystem);
                location = $"[{carrier.Location.StarSystem}]({locUrl.AbsoluteUri})";
            }

            builder.AddField("Location", location, true);
            builder.AddField("Owner", owner.Mention, true);
            builder.AddField("Status", carrier.Status.ToString(), true);

            if (jump is not null && jump.Status is JumpStatus.Plotted or JumpStatus.Lockdown)
            {
                var distance = jump.Distance.HasValue ? $"({jump.Distance.Value} ly)" : "";
                builder.AddField("Destination", $"[{jump.Destination}]({new Uri(Constants.Inara.StarSystemBaseUrl + jump.Destination).AbsoluteUri}) {distance}", true);
                builder.AddField("Departure", $"{Formatter.Timestamp(jump.JumpTime, TimestampFormat.ShortDateTime)} ({Formatter.Timestamp(jump.JumpTime)})", true);
                builder.AddField("Lockdown", jump.IsPastLockdownTime() ? "Active" : $"{Formatter.Timestamp(jump.JumpTime.Subtract(jump.LockdownAt))}", true);
            }

            var passengerString = string.Join(", ", carrier.Passengers.Select(p => $"<@{p}>"));
            builder.AddField("Passengers", string.IsNullOrEmpty(passengerString) ? Formatter.Italic("None") : passengerString);

            builder.WithColor(new DiscordColor(carrier.Color));

            await ctx.CreateResponseAsync(builder);
        }

        [SlashCommand("location", "Manually set the location of a carrier")]
        public async Task MethodName(InteractionContext ctx, [Option("star", "The name of the star system")] string star,
            [Option("carrier", "The id of the carrier."), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId = null,
            [Option("body", "The name of the body within the system the carrier is orbiting")] string body = null)
        {
            var carrier = carrierId is null ? carrierService.GetCarrierByChannel(ctx.Channel) : carrierService.GetCarrier(carrierId);
            if (carrier is null)
            {
                await ctx.CreateResponseAsync("Carrier not found", ephemeral: true);
            }

            var location = new Location
            {
                StarSystem = star,
                Body = body
            };

            carrier.Location = location;
            carrierService.UpdateCarrier(carrier);

            await ctx.CreateResponseAsync("Location set.", ephemeral: true);
        }

        /// <summary>
        /// Provides commands for managing carrier jumps.
        /// </summary>
        [SlashCommandGroup("jump", "Manage and announce carrier jumps.")]
        public class JumpCommands : ApplicationCommandModule
        {
            public MongoClient mongoClient { private get; set; }
            public CarrierService carrierService { private get; set; }
            public JumpService jumpService { private get; set; }

            [SlashCommand("plotted", "Announce that a carrier jump has been plotted.")]
            [RequireCarrierOwner]
            public async Task Plotted(InteractionContext ctx, [Option("carrier", "The id of the carrier plot a jump for."), Autocomplete(typeof(OwnedCarrierAutocompleteProvider))] string carrierId,
            [Option("destination", "The name of the system the carrier is jumping to.")] string destination,
            [Option("distance", "The jump distance in light years")] double? distance = null,
            [Option("eta", "Amount of time left until the jump in MM:SS format. Defaults to 15 minutes.")] string eta = "15:00")
            {
                var timeSpan = TimeSpan.ParseExact(eta, @"mm\:ss", CultureInfo.InvariantCulture);

                var jump = new Jump()
                {
                    CarrierId = carrierId,
                    Destination = destination,
                    JumpTime = DateTime.Now.Add(timeSpan),
                    Distance = distance
                };

                //var carrierCollection = mongoClient.GetCollection<Carrier>();
                var carrier = carrierService.GetCarrier(carrierId);
                if (carrier is null)
                {
                    await ctx.CreateResponseAsync("Carrier not found", ephemeral: true);
                    return;
                }

                jumpService.AddJump(jump);
                jumpService.SendJumpAnnouncement(jump);

                await ctx.CreateResponseAsync("Jump plotted", ephemeral: true);
            }

            [SlashCommand("complete", "Announce that a carrier jump has completed.")]
            [RequireCarrierOwner]
            public async Task Complete(InteractionContext ctx, [Option("carrier", "The id of the carrier."), Autocomplete(typeof(OwnedCarrierAutocompleteProvider))] string carrierId)
            {
                var jump = jumpService.GetLatestJumpForCarrier(carrierId);
                if (jump is not null && jump.Status is not JumpStatus.Complete)
                {
                    jump.Status = JumpStatus.Complete;
                    jump.JumpTime = DateTime.UtcNow;
                    jumpService.UpdateJump(jump);
                    jumpService.UpdateJumpAnnouncement(jump);
                    await ctx.CreateResponseAsync("Jump marked complete", ephemeral: true);
                    return;
                }

                await ctx.CreateResponseAsync("Jump not found or is already complete", ephemeral: true);
            }

            [SlashCommand("cancelled", "Announce that a carrier jump has been cancelled")]
            [RequireCarrierOwner]
            public async Task Cancelled(InteractionContext ctx, [Option("carrier", "The id of the carrier."), Autocomplete(typeof(OwnedCarrierAutocompleteProvider))] string carrierId)
            {
                var jump = jumpService.GetLatestJumpForCarrier(carrierId);
                if (jump is not null && jump.Status is not JumpStatus.Complete)
                {
                    jump.Status = JumpStatus.Cancelled;
                    jumpService.UpdateJump(jump);
                    jumpService.UpdateJumpAnnouncement(jump);
                    await ctx.CreateResponseAsync("Jump cancelled", ephemeral: true);
                }
                else
                {
                    await ctx.CreateResponseAsync("Jump not found or is already complete", ephemeral: true);
                }
            }

            [SlashCommand("eta", "Override the Jump ETA for a plotted carrier jump")]
            [RequireCarrierOwner]
            public async Task Eta(InteractionContext ctx, [Option("carrier", "The id of the carrier."), Autocomplete(typeof(OwnedCarrierAutocompleteProvider))] string carrierId,
                [Option("countdown", "Amount of time left until jump in mm:ss format.")] string countdown)
            {
                TimeSpan eta;
                var valid = TimeSpan.TryParseExact(countdown, @"mm\:ss", CultureInfo.InvariantCulture, out eta);
                if (valid)
                {
                    var jump = jumpService.GetLatestJumpForCarrier(carrierId);
                    if (jump is not null && jump.Status is not JumpStatus.Cancelled)
                    {
                        jump.JumpTime = DateTime.UtcNow + eta;
                        jumpService.UpdateJump(jump);
                        jumpService.UpdateJumpAnnouncement(jump);
                        await ctx.CreateResponseAsync("Jump eta updated", ephemeral: true);
                    }
                    else
                    {
                        await ctx.CreateResponseAsync("There is no active jump for the specified carrier", ephemeral: true);
                    }
                }
                else
                {
                    await ctx.CreateResponseAsync("Invalid countdown format.", ephemeral: true);
                }
            }
        }

        /// <summary>
        /// Provides commands for configuring fleet carriers.
        /// </summary>
        [SlashCommandGroup("config", "Configure various carrier related aspects of the bot")]
        [RequireCarrierOwner]
        class CarrierConfigureCommands : ApplicationCommandModule
        {
            public CarrierService carrierService { private get; set; }

            [SlashCommand("channel", "Map a channel to a registered carrier")]
            public async Task SetCarrierChannel(InteractionContext ctx, [Option("carrier", "The id of the carrier to configure"), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId,
                [Option("channel", "The channel to map to. Defaults to the current channel"), ChannelTypes(ChannelType.Text)] DiscordChannel channel = null)
            {
                var botMember = ctx.Guild.CurrentMember;

                if (channel == null)
                {
                    channel = ctx.Channel;
                }

                var carrier = carrierService.GetCarrier(carrierId);

                if (carrier == null)
                {
                    await ctx.CreateResponseAsync("Carrier not found", ephemeral: true);
                    return;
                }

                //Vaildate bot permissions in the target channel.
                //Note: This gets the channel directly from the guild, as permissions are not accurate from the command arugment
                var permissions = ctx.Guild.GetChannel(channel.Id).PermissionsFor(botMember);
                
                var missingPermissions = new List<string>();
                foreach (var requiredPermission in Constants.CarrierChannelPermissions)
                {
                    if (!permissions.HasPermission(requiredPermission)) {
                        missingPermissions.Add(requiredPermission.ToPermissionString());
                    }
                }

                if (missingPermissions.Count > 0) {
                    var permString = string.Join(", ", missingPermissions.Select(s => Formatter.InlineCode(s)));
                    var builder = new DiscordInteractionResponseBuilder();
                    builder.AsEphemeral();
                    builder.WithContent($"Unable to set carrier channel to {channel.Mention}: Missing the following permissions: {permString}");
                    Log.Warning($"Unable to set carrier channel to {channel.Id} due to missing permissions: {string.Join(',', missingPermissions)}");

                    await ctx.CreateResponseAsync(builder);
                    return;
                }

                carrier.Channel = new CarrierChannel
                {
                    GuildId = (ulong)channel.GuildId,
                    ChannelId = channel.Id,
                    BotCreated = false
                };

                carrierService.UpdateCarrier(carrier);

                await ctx.CreateResponseAsync($"Carrier channel set to {channel.Mention}.", ephemeral: true);
            }

            [SlashCommand("color", "Set the embed color for a carrier")]
            public async Task SetCarrierColor(InteractionContext ctx, [Option("carrier", "The id of the carrier to configure"), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId,
                [Option("color", "The color to use in embeds for this carrier"), Autocomplete(typeof(ColorAutocompleteProvider))] string color)
            {
                var carrier = carrierService.GetCarrier(carrierId);
                if (carrier is null)
                {
                    await ctx.CreateResponseAsync("Carrier not found", ephemeral: true);
                }

                carrier.Color = color;
                carrierService.UpdateCarrier(carrier);

                await ctx.CreateResponseAsync("Carrier color set to " + color, ephemeral: true);
            }
        }
    }
}