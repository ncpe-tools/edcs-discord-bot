using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.SlashCommands;
using EDCS.Services;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace EDCS.Commands.Attributes
{
    /// <summary>
    /// Require the carrier owner or administrator to use a slash command. Checks for either a "carrier" arugment or if the current channel is mapped to a carrier.
    /// </summary>
    class RequireCarrierOwnerAttribute : SlashCheckBaseAttribute
    {

        public override Task<bool> ExecuteChecksAsync(InteractionContext ctx)
        {
            return Task.Run(() =>
            {
                if (ctx.Guild == null)
                {
                    return false;
                }

                var permissions = ctx.Channel.PermissionsFor(ctx.Member);

                var allowed = permissions.HasPermission(Permissions.Administrator);

                var carrierService = ctx.Services.GetService<CarrierService>();

                // Get a list of all interaction options, including nested ones from subcommands
                var options = ctx.Interaction.Data.Options;
                foreach (var opt in ctx.Interaction.Data.Options)
                {
                    if (opt.Options is not null) {
                        options = options.Concat(opt.Options);
                    }
                }

                var carrierIdOption = options?.Where(x => x.Name.Equals("carrier")).SingleOrDefault();

                if (carrierIdOption != null)
                {
                    var carrierId = (string) carrierIdOption.Value;
                    var carrier = carrierService.GetCarrier(carrierId);
                    allowed = allowed || (carrier?.OwnerId == ctx.Member.Id);
                }
                else
                {
                    var carrier = carrierService.GetCarrierByChannel(ctx.Channel);
                    allowed = allowed || carrier?.OwnerId == ctx.Member.Id;
                }

                return allowed;
            });
        }
    }
}