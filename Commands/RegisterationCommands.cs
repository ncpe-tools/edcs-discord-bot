using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using EDCS.Commands.Attributes;
using EDCS.Commands.Autocomplete;
using EDCS.Model;
using EDCS.Services;
using Microsoft.Extensions.Logging;

namespace EDCS.Commands
{
    /// <summary>
    /// Provides commands related to fleet carrier registration.
    /// </summary>
    class RegistrationCommands : ApplicationCommandModule
    {
        public CarrierService carrierService { private get; set; }

        [SlashCommand("register", "Register a carrier with you as the owner.")]
        public async Task Register(InteractionContext ctx,
        [Option("carrier", "The in-game ID of the carrier to add.")] string carrierId,
        [Option("name", "The name of the carrier to display in chat.")] string name,
        [Option("create-channel", "Whether or not to automatially create a channel for the bot.")] bool createChannel,
        [Option("category", "The category under which to create the carrier's channel"), ChannelTypes(ChannelType.Category)] DiscordChannel category = null,
        [Option("color", "The color to use in embeds for this carrier"), Autocomplete(typeof(ColorAutocompleteProvider))] string color = "#e3e5e8")
        {
            var carrierRegex = new Regex(@"[A-Z0-9]{3}-[A-Z0-9]{3}");

            if (!carrierRegex.Match(carrierId).Success)
            {
                await ctx.CreateResponseAsync("Invalid carrier id.", ephemeral: true);
            }

            var carrier = new Carrier();
            carrier.Id = carrierId;
            carrier.OwnerId = ctx.Member.Id;
            carrier.Name = name;
            carrier.GuildId = ctx.Guild.Id;

            if (carrierService.CarrierExists(carrierId))
            {
                await ctx.CreateResponseAsync("Carrier already exists", ephemeral: true);
            }

            DiscordChannel channel = null;
            if (createChannel)
            {
                channel = await ctx.Guild.CreateChannelAsync(name, ChannelType.Text, category);

                carrier.Channel = new CarrierChannel()
                {
                    ChannelId = channel.Id,
                    GuildId = (ulong)channel.GuildId,
                    BotCreated = true
                };
            }

            carrierService.RegsiterCarrier(carrier);

            var msg = $"The carrier **{carrierId}** has been registered.";
            if (channel != null)
            {
                msg += $" The channel {Formatter.Mention(channel)} has automatically been created for it.";
            }

            var builder = new DiscordInteractionResponseBuilder();
            builder.WithContent(msg);

            await ctx.CreateResponseAsync(DSharpPlus.InteractionResponseType.ChannelMessageWithSource, builder);
        }

        [SlashCommand("unregister", "Unregister a carrier from the bot. Any autocreated channels will be deleted.")]
        [RequireCarrierOwner]
        public async Task Unregister(InteractionContext ctx, [Option("carrier", "The in-game ID of the carrier to remove."), Autocomplete(typeof(CarrierAutocompleteProvider))] string carrierId)
        {
            var carrierRegex = new Regex(@"[A-Z0-9]{3}-[A-Z0-9]{3}");

            if (!carrierRegex.Match(carrierId).Success)
            {
                await ctx.CreateResponseAsync("Invalid carrier id.", ephemeral: true);
                return;
            }

            var carrier = carrierService.GetCarrier(carrierId);

            if (carrier == null)
            {
                await ctx.CreateResponseAsync("Carrier does not exist", ephemeral: true);
                return;
            }

            var builder = new DiscordInteractionResponseBuilder();
            builder.WithContent($"Are you sure you want to unregsiter {Formatter.Bold(carrier.Name)}? This will delete any autocreated channels and cannot be undone.");
            builder.AddComponents(new DiscordButtonComponent(ButtonStyle.Danger, "unregsiter_confirm", "Confirm"));
            builder.AsEphemeral(true);

            await ctx.CreateResponseAsync(builder);

            var response = await ctx.GetOriginalResponseAsync();

            // Wait for the confirm button to be pressed. Timesout after 5 minutes.
            var confirm = await response.WaitForButtonAsync(ctx.User, TimeSpan.FromMinutes(5));

            if (!confirm.TimedOut && confirm.Result.Id == "unregsiter_confirm")
            {
                if (ctx.Guild.Id == carrier.Channel.GuildId && carrier.Channel.BotCreated)
                {
                    try
                    {
                        var discordChannel = ctx.Guild.GetChannel(carrier.Channel.ChannelId);
                        if (discordChannel != null)
                        {
                            await discordChannel.DeleteAsync();
                        }
                    }
                    catch (ServerErrorException ex)
                    {
                        ctx.Client.Logger.LogDebug(ex.Message);
                    }
                }

                carrierService.UnregisterCarrier(carrierId);
                await confirm.Result.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DiscordInteractionResponseBuilder().WithContent($"Carrier {carrier.Id} unregistered."));
            }
            else { }

        }

    }

}