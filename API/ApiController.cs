using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using EDCS.Model;
using EDCS.DTO;
using EDCS.Services;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace EDCS.Api
{
    [ApiController]
    [Route("api")]
    public class ApiController : ControllerBase
    {
        private readonly MongoClient _mongo;
        private readonly CarrierService _carrierService;
        private readonly JumpService _jumpService;

        public ApiController(MongoClient mongo, CarrierService carrierService, JumpService jumpService) {
            _mongo = mongo;
            _carrierService = carrierService;
            _jumpService = jumpService;
        }

        [HttpGet("ping")]
        public ActionResult Ping() {
            return Ok();
        }

        [HttpGet("carriers")]
        public IEnumerable GetCarriers() {
            var carriers = _carrierService.GetAllCarriers();
            return carriers.Select(c => new {
                Id = c.Id,
                Name = c.Name,
                Owner = c.OwnerId,
                Location = c.Location,
                Access = c.Access.ToString()
            });
        }

        [HttpGet("carrier/{id}")]
        public ActionResult<Carrier> GetCarrier(string id) {
            var carrier = _carrierService.GetCarrier(id);
            return carrier is not null ? carrier : NotFound("Carrier is not registered");
        }

        [HttpPost("carrier/stats")]
        public ActionResult<CarrierStatsDTO> UpdateCarrierStats(CarrierStatsDTO stats) {
            var carrier = _carrierService.GetCarrier(stats.Callsign);
            if (carrier is null) {
                return NotFound("Carrier is not registered");
            }

            carrier.FuelLevel = stats.FuelLevel;
            carrier.Access = _carrierService.ConvertDockingAccessEnum(stats.DockingAccess);
            carrier.MarketId = stats.CarrierId;
            _carrierService.UpdateCarrier(carrier);
            return Ok(stats);
        }

        [HttpPost("carrier/materials")]
        public ActionResult<FCMaterialsDTO> UpdateCarrierMaterials(FCMaterialsDTO materials) {
            var carrier = _carrierService.GetCarrierByFilter(c => c.MarketId == materials.MarketId);
            if (carrier is null) {
                return NotFound("Carrier is not registered");
            }

            carrier.Materials = materials.Items;
            carrier.MaterialsLastUpdated = DateTime.Parse(materials.Timestamp);
            _carrierService.UpdateCarrier(carrier);
            return Ok(materials);
        }

        [HttpPost("carrier/{id}/jump")]
        public ActionResult<Jump> PostJump(string id, string destination) {
            var carrier = _carrierService.GetCarrier(id);
            if (carrier is null) {
                return NotFound("Carrier is not registered");
            }

            var jump = new Jump {
                Destination = destination,
                CarrierId = id,
                JumpTime = DateTime.UtcNow.AddMinutes(15)
            };
            _jumpService.AddJump(jump);
            _jumpService.SendJumpAnnouncement(jump);
            return Ok(jump);
        }

        [HttpPost("carrier/jump")]
        public ActionResult<CarrierJumpRequestDTO> PostJump(CarrierJumpRequestDTO jumpRequest) {
            var carrier = _carrierService.GetCarrier(jumpRequest.CarrierId);
            if (carrier is null) {
                return NotFound("Carrier is not registered");
            }

            var jump = new Jump {
                JumpTime = DateTime.UtcNow.AddMinutes(15),
                Source = "API",
                Destination = jumpRequest.SystemName,
                DestinationBody = jumpRequest.Body,
                CarrierId = carrier.Id
            };
            _jumpService.AddJump(jump);
            _jumpService.SendJumpAnnouncement(jump);
            return Ok(jumpRequest);
        }

        [HttpDelete("carrier/{id}/jump")]
        public ActionResult CancelJump(ulong id) {
            var carrier = _carrierService.GetCarrier(id);
            if (carrier is not null) {
                return NotFound("Carrier is not registered");
            }

            var jump = _jumpService.GetLatestJumpForCarrier(carrier.Id);
            if (jump is not null) {
                return NotFound("No jump plotted");
            }

            jump.Status = JumpStatus.Cancelled;
            _jumpService.UpdateJump(jump);
            _jumpService.UpdateJumpAnnouncement(jump);
            return Ok();
        }
    }
}
