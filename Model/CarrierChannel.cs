using MongoDB.Bson.Serialization.Attributes;

namespace EDCS.Model
{
    [BsonIgnoreExtraElements]
    public class CarrierChannel
    {
        public ulong ChannelId { get; set; }
        public ulong GuildId { get; set; }
        public bool BotCreated { get; set; }
        public bool SilentRunning {get; set;}
        
    }
}