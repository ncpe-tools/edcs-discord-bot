using System;

namespace EDCS.Model
{
    /// <summary>
    /// Represents a Commander.
    /// </summary>
    public class Commander
    {
        /// <summary>
        /// Gets or sets the commander's Discord ID
        /// </summary>
        public ulong DiscordId {get; set;}

        /// <summary>
        /// Gets or sets the commander's in-game name.
        /// </summary>
        public string Name {get; set;}
        
        /// <summary>
        /// Gets or sets the commander's in-game location.
        /// </summary>
        public Location Location {get; set;}
    }
}