using System;
using System.Numerics;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;

namespace EDCS
{
    /// <summary>
    /// Represents a location in Elite Dangerous.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class Location
    {
        /// <summary>
        /// Gets or sets the name of the star system.
        /// </summary>
        public string StarSystem {get; set;}

        /// <summary>
        /// Gets or sets the name of the planet or station.
        /// </summary>
        public string Body {get; set;}

        /// <summary>
        /// Gets or sets the galactic position of the system relative to Sol.
        /// </summary>
        [BsonSerializer(typeof(BsonNullableVectorSerializer))]
        public Vector3? Position {get; set;}

        /// <summary>
        /// Gets or sets the latitude if near or landed on a planet.
        /// </summary>
        public float Latitude {get; set;}

        /// <summary>
        /// Gets or sets the longitute if near or landed on a planet.
        /// </summary>
        public float Longitude {get; set;}

        /// <summary>
        /// Gets or sets the body type.
        /// </summary>
        public BodyType Type {get; set;}
        
        public LocationState State {get; set;}

        /// <summary>
        /// Gets or sets the source of the location information.
        /// </summary>
        /// <remarks>This is intended to record if the location information was fetched from an integration (ie, inara.cz), from a command, or from a player journal update.</remarks>
        public string Source {get; set;}

        public override string ToString()
        {
            
            return (Body != null) ? $"{StarSystem} ({Body})" : $"{StarSystem}" ;
        }

        public enum BodyType 
        {
            Star,
            Planet,
            Station,
            Carrier
        }

        public enum LocationState 
        {
            InSpace, 
            Docked,
            Landed,
            OnFoot
        }
    }
}