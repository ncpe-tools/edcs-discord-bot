using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.Json.Serialization;
using DSharpPlus.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace EDCS.Model
{
    /// <summary>Represents a registered Fleet Carrier.</summary>
    [BsonIgnoreExtraElements]
    public class Carrier
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId MongoId { get; set; }

        /// <summary>Hidden number ID for the carrier</summary>
        public ulong MarketId {get; set;}

        /// <summary>Gets or sets the carrier's in-game ID.</summary>
        public string Id {get; set;}

        /// <summary>Gets or sets the related channel for the carrier.</summary>
        public CarrierChannel Channel {get; set;}

        /// <summary>Gets or sets the Discord ID of the carrier's owner.</summary>
        /// <value></value>
        public ulong OwnerId {get; set;}

        /// <summary>Gets or sets the name of the carrier.</summary>
        public string Name {get; set;}

        public HashSet<ulong> Passengers {get; set;} = new HashSet<ulong>();
        public HashSet<ulong> DockedPlayers {get; set;} = new HashSet<ulong>();

        /// <summary>Gets or sets the guild id of the Discord server the carrier was registered in.</summary>
        public ulong GuildId {get; set;}

        /// <summary>Gets or sets the current location of the carrier.</summary>
        public Location Location {get; set;}

        /// <summary>Gets or sets the current status of the carrier.</summary>
        public CarrierStatus Status {get; set;}

        /// <summary>Gets or sets the docking access level for the carrier.</summary>
        public CarrierAccess Access {get; set;}

        /// <summary>Gets or sets the current fuel level of the carrier.</summary>
        public int FuelLevel {get; set;}

        public string Color {get; set;} = DiscordColor.Grayple.ToString();

        public MaterialEntry[] Materials {get; set;}

        public DateTime MaterialsLastUpdated {get; set;}

        /// <summary>
        /// Checks if the carrier has a passenger with the given Discord ID.
        /// </summary>
        /// <param name="user">The ID to check</param>
        /// <returns><c>true</c> if the user is a passenger; otherwise <c>false</c></returns>
        public bool HasPassenger(ulong user)
        {
            return Passengers.Contains(user);
        }

        /// <summary>
        /// Add a passeneger to the carrier.
        /// </summary>
        /// <param name="user">The id of the user to add as a passenger.</param>
        /// <returns><c>true</c> if the user was added; <c>false</c> if the user is already a passenger on the carrier.</returns>
        public bool AddPassenger(ulong user)
        {
            return Passengers.Add(user);
        }

        /// <param name="user">The user to add as a passenger.</param>
        /// <inheritdoc cref="AddPassenger(ulong)"/>
        public bool AddPassenger(Commander user) {
            return Passengers.Add(user.DiscordId);
        }

        /// <summary>
        /// Removes a passenger from the carrier.
        /// </summary>
        /// <param name="passenger">The ID of the passenger to remove.</param>
        /// <returns><c>true</c> if the commander was removed; <c>false</c> if the commander was not a passenger.</returns>
        public bool RemovePassenger(ulong passenger)
        {
            return Passengers.Remove(passenger);
        }

        /// <summary>
        /// Gets the Inara.cz url of the carrier as a string.
        /// </summary>
        /// <returns>The inara.cz url of the carrier.</returns>
        public string GetInaraUrl() {
            return Constants.Inara.StationBaseUrl + Id;
        }

    }

    [BsonIgnoreExtraElements]
    public class MaterialEntry {
        public uint Price {get; set;}
        public uint Demand {get; set;}
        public uint Stock {get; set;}
        public string Name {get; set;}
    }

    /// <summary>
    /// Represents the in-game status of a fleet carrier.
    /// </summary>
    public enum CarrierStatus
    {
        Active,
        JumpPlotted,
        InLockdown,
        Jumping,
        Deactivated
    }

    /// <summary>
    /// Represents the docking access level of a fleet carrier.
    /// </summary>
    public enum CarrierAccess
    {
        All,
        None,
        Friends,
        Squadron,
        SquadronFriends,
        Unknown
    }
}
