using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Serilog;

namespace EDCS.Model
{
    /// <summary>
    /// Represents a plotted fleet carrier jump.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class Jump
    {
        /// <summary>The point in time before the carrier jumps when lockdown is activated.</summary>
        internal readonly TimeSpan LockdownAt = new TimeSpan(0, 3, 20);

        public ObjectId Id {get; set;}

        /// <summary>
        /// Gets or sets the name of the system the carrier is jumping from.
        /// </summary>
        public string Origin {get; set;}

        /// <summary>
        /// Gets or sets the name of the system the carrier is jumping to.
        /// </summary>
        public string Destination {get; set;}

        /// <summary>
        /// Gets or sets the name of the body the carrier is jumping to.
        /// </summary>
        public string DestinationBody {get; set;}

        /// <summary>
        /// Gets or sets the ID of the carrier.
        /// </summary>
        public string CarrierId {get; set;}

        /// <summary>
        /// Gets or sets the estimated time the jump will occur.
        /// </summary>
        public DateTime JumpTime {get; set;}

        /// <summary>
        /// Gets or sets the ID of the Discord message announcing the jump.
        /// </summary>
        /// <value></value>
        public ulong MessageId {get; set;}

        /// <summary>
        /// Gets or sets the status of the jump.
        /// </summary>
        /// <value></value>
        public JumpStatus Status {get; set;}

        /// <summary>
        /// Gets or sets the source of the jump.
        /// </summary>
        public string Source {get; set;}

        /// <summary>
        /// Gets or sets the distance in lightyears of the jump.
        /// </summary>
        public double? Distance {get; set;}

        /// <summary>
        /// Checks if the carrier has entered lockdown for the jump.
        /// </summary>
        /// <returns><c>true</c> if the current time is after the lockdown time for the jump; otherwise <c>false</c></returns>
        public bool IsPastLockdownTime() {
            Log.Debug($"{DateTime.UtcNow} {JumpTime.Subtract(LockdownAt)}");
            return DateTime.UtcNow >= JumpTime.Subtract(LockdownAt);
        }

        /// <summary>
        /// Checks if the estimated time for the jump has passed.
        /// </summary>
        /// <returns><c>true</c> if the current time has passed the <see cref="JumpTime"/>; otherwise <c>false</c></returns>
        public bool IsPastCompleteTime() {
            return DateTime.UtcNow > JumpTime;
        }

    }

    /// <summary>
    /// Represents the status of a carrier jump.
    /// </summary>
    public enum JumpStatus 
    {
        Plotted,
        Lockdown,
        Jumping,
        Complete,
        Cancelled,
        Unknown
    }
}