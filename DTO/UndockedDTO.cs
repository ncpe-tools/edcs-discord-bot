namespace EDCS.DTO {

    /// <suymmary>
    /// DTO for Undocked journal event
    /// https://elite-journal.readthedocs.io/en/latest/Travel/#undocked
    /// </summary>
    public class UndockedDTO {
        public string StationName {get; set;}

        public ulong MarketId {get; set;}
    }
}