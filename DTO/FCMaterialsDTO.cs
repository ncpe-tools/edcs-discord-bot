using EDCS.Model;

namespace EDCS.DTO {

    public class FCMaterialsDTO {
        public ulong MarketId {get; set;}

        public string Timestamp {get; set;}

        public MaterialEntry[] Items {get; set;}
    }
}
