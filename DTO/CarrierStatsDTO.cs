namespace EDCS.DTO
{
    /// <summary>
    /// DTO for CarrierStats journal event 
    /// https://elite-journal.readthedocs.io/en/latest/Fleet%20Carriers/#carrierstats
    /// </summary>
    public class CarrierStatsDTO
    {
        public int FuelLevel {get; set;}

        public string Name {get; set;}

        public ulong CarrierId {get; set;}

        public string Callsign {get; set;}

        public string DockingAccess {get; set;}

        public bool AllowNotorious {get; set;}

        public CrewService[] Crew {get; set;}

        public FinanceInfo Finance {get; set;}

        public PackInfo[] ShipPacks {get; set;}

        public PackInfo[] ModulePacks {get; set;}
    }

    public class CrewService {
        public string CrewRole {get; set;}

        public bool Activated {get; set;}

        public bool Enabled {get; set;}

        public string CrewName {get; set;}
    }

    public class FinanceInfo {
        public long CarrierBalance {get; set;}

        public long ReserveBalance {get; set;}

        public long AvailableBalance {get; set;}

        public int ReservePercent {get; set;}

        public int TaxRate_rearm {get; set;}

        public int TaxRate_refuel {get; set;}

        public int TaxRate_repair {get; set;}

        public int TaxRate_pioneersupplies {get; set;}

        public int TaxRate_outfitting {get; set;}

        public int TaxRate_shipyard {get; set;}
    }

    public class PackInfo {
        public string PackTheme {get; set;}

        public int PackTier {get; set;}
    }
}