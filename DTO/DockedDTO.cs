namespace EDCS.DTO {

    /// <summary>
    /// DTO for Docked journal event
    /// https://elite-journal.readthedocs.io/en/latest/Travel/#docked
    /// </summary>
    public class DockedDTO {
        public ulong MarketId {get; set;}

        public string StationName {get; set;}

        public string StarSystem {get; set;}
    }
}