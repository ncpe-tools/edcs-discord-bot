namespace EDCS.DTO {

    /// <summary>
    /// DTO for CarrierJumpRequest journal event
    /// https://elite-journal.readthedocs.io/en/latest/Fleet%20Carriers/#carrierjumprequest
    /// </summary>
    public class CarrierJumpRequestDTO {
        public ulong CarrierId {get; set;}

        public string SystemName {get; set;}

        public string Body {get; set;}
    }
}