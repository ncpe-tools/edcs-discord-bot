using System;
using DSharpPlus;

namespace EDCS
{
    public static class Constants
    {
        public static class Inara 
        {
            public static readonly string StarSystemBaseUrl = "https://inara.cz/starsystem/?search=";
            public static readonly string StationBaseUrl = "https://inara.cz/station/?search=";
        }

        public static class Emoji 
        {
            public static readonly string FCJump = ":fc_jump:";
            public static readonly string FCDone = ":fc_done:";
            public static readonly string FCCancel = ":fc_cancel:";
            public static readonly string FCLockdown = ":fc_lockdown:";
        }

        public static readonly Permissions[] CarrierChannelPermissions = {
            Permissions.ManageChannels,
            Permissions.AccessChannels,
            Permissions.SendMessages,
            Permissions.ReadMessageHistory,
            Permissions.UseExternalEmojis,
        };
    }
}