using System;
using Humanizer;
using MongoDB.Driver;

namespace EDCS
{
    /// <summary>
    /// Represents a wrapper around <see cref="MongoDB.Driver.MongoClient"/>.
    /// </summary>
    public class MongoClient
    {
        public MongoDB.Driver.MongoClient InnerClient {get; private set;}
        private string DatabaseName {get; set;}
        
        /// <summary>
        /// Initializes a new instance of <see cref="MongoClient"/>.
        /// </summary>
        /// <param name="connectionUri">The connection url to pass to the internal MongoClient.</param>
        /// <param name="database">The name of the database to connect to.</param>
        public MongoClient(string connectionUri, string database) {
            InnerClient = new MongoDB.Driver.MongoClient(connectionUri);
            DatabaseName = database;
            
        }

        /// <summary>
        /// Gets a collection.
        /// </summary>
        /// <returns>An implementation of a collection.</returns>
        /// <remarks>The name of the collection will be a pluralized form of the class name.</remarks>
        public IMongoCollection<T> GetCollection<T>() 
        {
            var collectionName = typeof(T).Name.Pluralize().ToLower();
            return InnerClient.GetDatabase(DatabaseName).GetCollection<T>(collectionName);
        }

        /// <summary>
        /// Gets the current database.
        /// </summary>
        /// <returns>The current database.</returns>
        public IMongoDatabase GetDatabase() {
            return InnerClient.GetDatabase(DatabaseName);
        }

    }
}